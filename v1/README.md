# Getting Started

### Prerequisite
* Oracle 12c
* DBA access to Oracle is available
* Application user created [The application code assumed the application user is named _**dbadmin**_]

### Create Initial Table

```
CREATE TABLE deal (
    id number primary key,    
    deal_ref varchar2(32) not null,    
    base_ccy varchar2(3) not null,
    quoted_ccy varchar2(3) not null,    
    deal_amount number(19,4) not null,    
    status varchar2(10) not null,    
    value_date timestamp(6) not null
);
```

### Enable Oracle EBR (Executed by DBA)

```
create edition app_v1 as child of ora$base;
alter user dbadmin enable editions;
grant use on edition app_v1 to dbadmin;
alter database default edition = app_v1;

```

### Create Editioning View (Executed by appuser)

```
alter table deal rename to deal_t;

create or replace editioning view deal as
select id, deal_ref, base_ccy, quoted_ccy, deal_amount, status, value_date
from   deal_t;
```