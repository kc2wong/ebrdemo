package com.bnpp.fxplus.ebrdemo.repository;

import com.bnpp.fxplus.ebrdemo.repository.entity.DealEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface DealRepository extends JpaRepository<DealEntity, Long> {

    Optional<DealEntity> findByDealRef(String dealRef);

}
