package com.bnpp.fxplus.ebrdemo.service;

import com.bnpp.fxplus.ebrdemo.repository.DealRepository;
import com.bnpp.fxplus.ebrdemo.repository.entity.DealEntity;
import com.bnpp.fxplus.ebrdemo.service.dto.DealDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static java.util.stream.Collectors.toList;

@Service
public class DealService {

    private final DealRepository dealRepository;

    public DealService(@Autowired DealRepository dealRepository) {
        this.dealRepository = dealRepository;
    }

    public DealDto createDeal(String baseCcy, String quotedCcy, BigDecimal dealAmount) {
        DealEntity dealEntity = new DealEntity();
        dealEntity.setId(System.nanoTime());
        dealEntity.setDealRef(UUID.randomUUID().toString().replaceAll("-", ""));
        dealEntity.setBaseCcy(baseCcy);
        dealEntity.setQuotedCcy(quotedCcy);
        dealEntity.setDealAmount(dealAmount);
        dealEntity.setStatus("NEW");
        dealEntity.setValueDate(OffsetDateTime.now().truncatedTo(ChronoUnit.DAYS));
        dealRepository.save(dealEntity);
        return entityToDto(dealEntity);
    }

    public Optional<DealDto> getByDealRef(String dealRef) {
        return dealRepository.findByDealRef(dealRef).map(this::entityToDto);
    }

    public List<DealDto> findAll() {
        return dealRepository.findAll().stream().map(this::entityToDto).collect(toList());
    }

    private DealDto entityToDto(DealEntity dealEntity) {
        DealDto dealDto = new DealDto();
        dealDto.setBaseCcy(dealEntity.getBaseCcy());
        dealDto.setDealAmount(dealEntity.getDealAmount());
        dealDto.setDealRef(dealEntity.getDealRef());
        dealDto.setId(dealEntity.getId());
        dealDto.setQuotedCcy(dealEntity.getQuotedCcy());
        dealDto.setStatus(dealEntity.getStatus());
        dealDto.setValueDate(dealEntity.getValueDate());
        return dealDto;
    }
}
