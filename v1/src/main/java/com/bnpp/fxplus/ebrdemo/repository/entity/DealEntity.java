package com.bnpp.fxplus.ebrdemo.repository.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.time.OffsetDateTime;

@Entity(name="deal")
public class DealEntity {

    @Column(name = "id")
    @Id
    private Long id;

    @Column(name = "deal_ref")
    private String dealRef;

    @Column(name = "base_ccy")
    private String baseCcy;

    @Column(name = "quoted_ccy")
    private String quotedCcy;

    @Column(name = "deal_amount")
    private BigDecimal dealAmount;

    @Column(name = "status")
    private String status;

    @Column(name = "value_date")
    private OffsetDateTime valueDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDealRef() {
        return dealRef;
    }

    public void setDealRef(String dealRef) {
        this.dealRef = dealRef;
    }

    public String getBaseCcy() {
        return baseCcy;
    }

    public void setBaseCcy(String baseCcy) {
        this.baseCcy = baseCcy;
    }

    public String getQuotedCcy() {
        return quotedCcy;
    }

    public void setQuotedCcy(String quotedCcy) {
        this.quotedCcy = quotedCcy;
    }

    public BigDecimal getDealAmount() {
        return dealAmount;
    }

    public void setDealAmount(BigDecimal dealAmount) {
        this.dealAmount = dealAmount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public OffsetDateTime getValueDate() {
        return valueDate;
    }

    public void setValueDate(OffsetDateTime valueDate) {
        this.valueDate = valueDate;
    }
}
