package com.bnpp.fxplus.ebrdemo.api;

import com.bnpp.fxplus.ebrdemo.api.model.Deal;
import com.bnpp.fxplus.ebrdemo.api.model.DealCreateRequest;
import com.bnpp.fxplus.ebrdemo.api.model.DealStatus;
import com.bnpp.fxplus.ebrdemo.service.DealService;
import com.bnpp.fxplus.ebrdemo.service.dto.DealDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

@Component
public class DealApiDelegateImpl implements DealApiDelegate {

    final DealService dealService;

    public DealApiDelegateImpl(@Autowired DealService dealService) {
        this.dealService = dealService;
    }

    @Override
    public ResponseEntity<Deal> v1DealsDealRefGet(String dealRef) {
        return dealService.getByDealRef(dealRef)
                .map(it -> ResponseEntity.ok(dtoToModel(it)))
                .orElse(ResponseEntity.notFound().build());
    }

    @Override
    public ResponseEntity<List<Deal>> v1DealsGet(Integer limit, Integer offset) {
        return ResponseEntity.ok(
                dealService.findAll()
                        .stream()
                        .map(this::dtoToModel)
                        .collect(toList())
        );
    }

    @Override
    public ResponseEntity<Deal> v1DealsPost(DealCreateRequest dealCreateRequest) {
        return ResponseEntity.ok(
                dtoToModel(dealService.createDeal(dealCreateRequest.getBaseCcy(), dealCreateRequest.getQuotedCcy(),
                        BigDecimal.valueOf(dealCreateRequest.getDealAmount())))
        );
    }

    private Deal dtoToModel(DealDto dealDto) {
        Deal deal = new Deal();
        deal.setBaseCcy(dealDto.getBaseCcy());
        deal.setDealAmount(Optional.ofNullable(dealDto.getDealAmount()).map(BigDecimal::doubleValue).orElse(null));
        deal.setDealRef(dealDto.getDealRef());
        deal.setQuotedCcy(dealDto.getQuotedCcy());
        deal.setStatus(Optional.ofNullable(dealDto.getStatus()).map(DealStatus::valueOf).orElse(null));
        deal.setValueDate(dealDto.getValueDate());
        return deal;
    }

}
