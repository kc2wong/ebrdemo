package com.bnpp.fxplus.ebrdemo.repository.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.time.OffsetDateTime;

@Entity(name="cumulative_deal_amount")
public class CumulativeDealAmountEntity {

    @Column(name = "id")
    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "total_deal_amount")
    private BigDecimal totalDealAmount;

    @Column(name = "value_date")
    private OffsetDateTime valueDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getTotalDealAmount() {
        return totalDealAmount;
    }

    public void setTotalDealAmount(BigDecimal totalDealAmount) {
        this.totalDealAmount = totalDealAmount;
    }

    public OffsetDateTime getValueDate() {
        return valueDate;
    }

    public void setValueDate(OffsetDateTime valueDate) {
        this.valueDate = valueDate;
    }
}
