package com.bnpp.fxplus.ebrdemo.service;

import com.bnpp.fxplus.ebrdemo.service.dto.DealDto;
import com.bnpp.fxplus.ebrdemo.service.exception.ApplicationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.event.TransactionPhase;
import org.springframework.transaction.event.TransactionalEventListener;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;

@Service
public class DealFacade {

    private static Logger logger = LoggerFactory.getLogger(DealFacade.class);

    @Value("${deal.thresholdAmount}")
    private BigDecimal thresholdAmount;

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    private final DealService dealService;

    public DealFacade(@Autowired DealService dealService) {
        this.dealService = dealService;
    }

    public DealDto createDealWithLimitCheck(String baseCcy, String quotedCcy, BigDecimal dealAmount) {
        OffsetDateTime valueDate = OffsetDateTime.now().truncatedTo(ChronoUnit.DAYS);
        BigDecimal totalDealAmount = dealService.updateCumulativeLimit(dealAmount, valueDate);
        logger.info("New totalDealAmount = {}", totalDealAmount);

        if (totalDealAmount.compareTo(thresholdAmount) > 0) {
            // Undo update cumulative amount when exceed threshold
            applicationEventPublisher.publishEvent(new UndoAddedCumulativeAmountEvent(dealAmount, valueDate));
            throw new ApplicationException(String.format("Cumulative deal amount %s exceed threshold %s", totalDealAmount, thresholdAmount), "CUMULATIVE_AMOUNT_EXCEED_MAX");
        }
        else {
            try {
                // create deal
                return dealService.createDeal(baseCcy, quotedCcy, dealAmount, valueDate);
            }
            catch (Exception e) {
                // Undo update cumulative amount when failed to create deal
                applicationEventPublisher.publishEvent(new UndoAddedCumulativeAmountEvent(dealAmount, valueDate));
                throw e;
            }
        }
    }

    @TransactionalEventListener(value = {UndoAddedCumulativeAmountEvent.class}, phase = TransactionPhase.AFTER_COMPLETION, fallbackExecution = true)
    public void handleOffsetCumulativeAmountEvent(UndoAddedCumulativeAmountEvent event) {
        dealService.updateCumulativeLimit(event.dealAmount.negate(), event.valueDate);
    }

    private static class UndoAddedCumulativeAmountEvent {
        BigDecimal dealAmount;
        OffsetDateTime valueDate;

        private UndoAddedCumulativeAmountEvent(BigDecimal dealAmount, OffsetDateTime valueDate) {
            this.dealAmount = dealAmount;
            this.valueDate = valueDate;
        }
    }
}
