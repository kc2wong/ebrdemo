package com.bnpp.fxplus.ebrdemo.api;

import com.bnpp.fxplus.ebrdemo.api.model.Deal;
import com.bnpp.fxplus.ebrdemo.api.model.DealCreateRequest;
import com.bnpp.fxplus.ebrdemo.service.DealFacade;
import com.bnpp.fxplus.ebrdemo.service.DealService;
import com.bnpp.fxplus.ebrdemo.service.dto.DealDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

@Component
public class DealApiDelegateImpl implements DealApiDelegate {

    final DealService dealService;
    final DealFacade dealFacade;

    public DealApiDelegateImpl(@Autowired DealService dealService, DealFacade dealFacade) {
        this.dealService = dealService;
        this.dealFacade = dealFacade;
    }

    @Override
    public ResponseEntity<Deal> v2DealsDealRefGet(String dealRef) {
        return dealService.getByDealRef(dealRef)
                .map(it -> ResponseEntity.ok(dtoToModel(it)))
                .orElse(ResponseEntity.notFound().build());
    }

    @Override
    public ResponseEntity<List<Deal>> v2DealsGet(Integer limit, Integer offset) {
        return ResponseEntity.ok(
                dealService.findAll()
                        .stream()
                        .map(this::dtoToModel)
                        .collect(toList())
        );
    }

    @Override
    public ResponseEntity<Deal> v2DealsPost(DealCreateRequest dealCreateRequest) {
        return ResponseEntity.ok(
                dtoToModel(dealFacade.createDealWithLimitCheck(dealCreateRequest.getBaseCcy(), dealCreateRequest.getQuotedCcy(),
                        BigDecimal.valueOf(dealCreateRequest.getDealAmount())))
        );

    }

    private Deal dtoToModel(DealDto dealDto) {
        Deal deal = new Deal();
        deal.setBaseCcy(dealDto.getBaseCcy());
        deal.setDealAmount(Optional.ofNullable(dealDto.getDealAmount()).map(BigDecimal::doubleValue).orElse(null));
        deal.setDealRef(dealDto.getDealRef());
        deal.setQuotedCcy(dealDto.getQuotedCcy());
        deal.setValueDate(dealDto.getValueDate());
        return deal;
    }

}
