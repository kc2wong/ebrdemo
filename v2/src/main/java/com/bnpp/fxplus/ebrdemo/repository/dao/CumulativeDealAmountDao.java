package com.bnpp.fxplus.ebrdemo.repository.dao;

import com.bnpp.fxplus.ebrdemo.repository.entity.CumulativeDealAmountEntity;
import com.bnpp.fxplus.ebrdemo.repository.entity.DealEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;
import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.Optional;

@Repository
public class CumulativeDealAmountDao {

    @Autowired
    private EntityManager entityManager;

    public BigDecimal updateCumulativeDealAmountOfValueDate(BigDecimal deltaAmount, OffsetDateTime valueDate) {
        StoredProcedureQuery query = entityManager
                .createStoredProcedureQuery("update_cumulative_deal_amount")
                .registerStoredProcedureParameter(1, BigDecimal.class, ParameterMode.IN)
                .registerStoredProcedureParameter(2, OffsetDateTime.class, ParameterMode.IN)
                .registerStoredProcedureParameter(3, BigDecimal.class, ParameterMode.OUT)
                .setParameter(1, deltaAmount)
                .setParameter(2, valueDate)
                ;
        query.execute();

        return (BigDecimal) query.getOutputParameterValue(3);
    }

}
