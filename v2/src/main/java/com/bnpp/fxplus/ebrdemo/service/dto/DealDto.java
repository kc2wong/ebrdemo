package com.bnpp.fxplus.ebrdemo.service.dto;

import java.math.BigDecimal;
import java.time.OffsetDateTime;

public class DealDto {

    private Long id;
    private String dealRef;
    private String baseCcy;
    private String quotedCcy;
    private BigDecimal dealAmount;
    private OffsetDateTime valueDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDealRef() {
        return dealRef;
    }

    public void setDealRef(String dealRef) {
        this.dealRef = dealRef;
    }

    public String getBaseCcy() {
        return baseCcy;
    }

    public void setBaseCcy(String baseCcy) {
        this.baseCcy = baseCcy;
    }

    public String getQuotedCcy() {
        return quotedCcy;
    }

    public void setQuotedCcy(String quotedCcy) {
        this.quotedCcy = quotedCcy;
    }

    public BigDecimal getDealAmount() {
        return dealAmount;
    }

    public void setDealAmount(BigDecimal dealAmount) {
        this.dealAmount = dealAmount;
    }

    public OffsetDateTime getValueDate() {
        return valueDate;
    }

    public void setValueDate(OffsetDateTime valueDate) {
        this.valueDate = valueDate;
    }
}
