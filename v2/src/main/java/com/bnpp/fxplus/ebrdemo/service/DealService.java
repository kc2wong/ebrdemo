package com.bnpp.fxplus.ebrdemo.service;

import com.bnpp.fxplus.ebrdemo.repository.DealRepository;
import com.bnpp.fxplus.ebrdemo.repository.dao.CumulativeDealAmountDao;
import com.bnpp.fxplus.ebrdemo.repository.entity.DealEntity;
import com.bnpp.fxplus.ebrdemo.service.dto.DealDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static java.util.stream.Collectors.toList;

@Service
public class DealService {

    private final DealRepository dealRepository;
    private final CumulativeDealAmountDao cumulativeDealAmountDao;

    public DealService(@Autowired DealRepository dealRepository, CumulativeDealAmountDao cumulativeDealAmountDao) {
        this.dealRepository = dealRepository;
        this.cumulativeDealAmountDao = cumulativeDealAmountDao;
    }

    public DealDto createDeal(String baseCcy, String quotedCcy, BigDecimal dealAmount, OffsetDateTime valueDate) {
        DealEntity dealEntity = new DealEntity();
        dealEntity.setId(System.nanoTime());
        dealEntity.setDealRef(UUID.randomUUID().toString().replaceAll("-", ""));
        dealEntity.setBaseCcy(baseCcy);
        dealEntity.setQuotedCcy(quotedCcy);
        dealEntity.setDealAmount(dealAmount);
        dealEntity.setValueDate(Optional.ofNullable(valueDate).orElseGet(OffsetDateTime::now).truncatedTo(ChronoUnit.DAYS));
        dealRepository.save(dealEntity);
        return entityToDto(dealEntity);
    }

    @Transactional(Transactional.TxType.REQUIRES_NEW)
    public BigDecimal updateCumulativeLimit(BigDecimal dealAmount, OffsetDateTime valueDate) {
        return cumulativeDealAmountDao.updateCumulativeDealAmountOfValueDate(dealAmount, valueDate);
    }

    public Optional<DealDto> getByDealRef(String dealRef) {
        return dealRepository.findByDealRef(dealRef).map(this::entityToDto);
    }

    public List<DealDto> findAll() {
        return dealRepository.findAll().stream().map(this::entityToDto).collect(toList());
    }

    private DealDto entityToDto(DealEntity dealEntity) {
        DealDto dealDto = new DealDto();
        dealDto.setBaseCcy(dealEntity.getBaseCcy());
        dealDto.setDealAmount(dealEntity.getDealAmount());
        dealDto.setDealRef(dealEntity.getDealRef());
        dealDto.setId(dealEntity.getId());
        dealDto.setQuotedCcy(dealEntity.getQuotedCcy());
        dealDto.setValueDate(dealEntity.getValueDate());
        return dealDto;
    }
}
