# Getting Started

### Create New Edition (Executed by DBA)

```
create edition app_v2 as child of app_v1;
grant use on edition app_v2 to dbadmin;

```

### Create Edition objects (Executed by appuser)

```
-- Switch to new edition
alter session set edition = app_v2;


-- Create new table and editioning view
create table cumulative_deal_amount_t (
    id number generated always as identity primary key,    
    total_deal_amount number(19,4) not null,    
    value_date timestamp(6) not null
);

create or replace editioning view cumulative_deal_amount as
select id, total_deal_amount, value_date
from   cumulative_deal_amount_t;

-- Create new stored procedure
create or replace procedure update_cumulative_deal_amount (
in_deal_amount in number,
in_value_date in timestamp,
out_deal_amount out number
) as
begin
  update cumulative_deal_amount
  set total_deal_amount = total_deal_amount + in_deal_amount
  where value_date = in_value_date
  returning total_deal_amount into out_deal_amount
  ;
end update_cumulative_deal_amount;

-- test default data
insert into cumulative_deal_amount (total_deal_amount, value_date) 
values 
(0, TIMESTAMP '2020-08-15 00:00:00 +08:00')

-- reverse cross edition trigger
create or replace trigger deal_status_rxe
before insert on deal_t
for each row
reverse crossedition
begin
  :NEW.status := 'NEW';
end deal_status_rxe;

```